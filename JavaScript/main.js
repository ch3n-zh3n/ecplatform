//Set transition to elements after load DOM
function transitionToAfterLoadElements() {
    var buttons = document.getElementsByClassName("after_load");
    for (i = 0; i < buttons.length; i++) {
        buttons[i].className += " transition";
    }
}

//DOM Loaded
onload = function () {    
    transitionToAfterLoadElements();
    disappearMessage();
};

//check full biographical access key 
function checkKey(element) {
    key = element.value;
    var xhr = new XMLHttpRequest();
    var url = "https://opcod3.com/Handlers/Ajax/checkKeyHandler.php?key=" + key;
    xhr.open("POST", url, true);
    xhr.timeout = 3000;
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById('placeholder').innerHTML = xhr.responseText;
        }

    };
}

//disappear message window
function disappearMessage() {
    element = document.getElementsByClassName("msg")[0];
    if (element !== undefined) {
        setTimeout(function () {
          document.getElementsByClassName("message-background")[0].className += " fadeout";
          element.className += " fadeout";
        }, 2500);
    }
}

// edit field
function editField(element) {
    unselectFields();
    baseText = element.innerHTML;
    id = element.parentNode.id;
    header = element.headers;
    if(element.childNodes[0].nodeName !== "INPUT") {
    element.innerHTML = "<input type='text' value='"+ baseText +"' onkeydown='saveFieldChanges(this,"+id+",header);' onFocusOut='unselectFields();'><input type='hidden' value='" + baseText + "'>";
    element.childNodes[0].focus();
    }
}

function saveFieldChanges(element,id,header) {
    if(window.event.keyCode === 13 || element.tagName === "SELECT") {
    newVal = element.value;
    
    var xhr = new XMLHttpRequest();
    var url = "https://opcod3.com/Handlers/Ajax/saveFieldChangesHandler.php?newVal=" + newVal + "&id=" +id+ "&header=" + header;
    xhr.open("POST", url, true);
    xhr.timeout = 3000;
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            if(xhr.responseText === "0") {
                if(element.tagName !== "SELECT") {
                element.parentNode.innerHTML = newVal;
                unselectFields();
                }
            } else {
                window.alert("Проблем! Този запис вече съществува.");
            }
        }
    };
}
}

function unselectFields() {
    var tdFields = document.getElementsByTagName("TD");
    
    for(i=0;i<tdFields.length;i++) {
        if(tdFields[i].childNodes[0].nodeName === "INPUT") {
            tdFields[i].innerHTML = tdFields[i].childNodes[1].value;
        }
    }
}