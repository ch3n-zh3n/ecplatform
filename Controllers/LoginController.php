<?php
require_once '../Core/Classes/Web.php';
require_once '../Core/Classes/DBManager.php';
require_once '../Core/Classes/Entities/Account.php';

$web = new Web();
$context = $web->context();

if($context->getParameter("logout")) {
    $context->removeAccount();
    header("Location: ".$web->getRoot());
    exit;
}

$username = $context->getParameter("username");
$pass = $context->getParameter("password");
$db = new DBManager();

if($username && $pass) {
    $account = $db->getAccountByUsername($username);
    if($account) {
        if($account->getPassword() != strtoupper(md5($pass))) {
           header("Location: ".$web->getRoot()."View/login.php?bad_credentials=TRUE");
           exit;
        } else {
           $context->setAccount($account);
           header("Location: ".$web->getRoot());
           exit;
        }
    }
}
header("Location: ".$web->getRoot()."View/login.php?bad_credentials=TRUE");