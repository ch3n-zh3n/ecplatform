<?php
require_once '../Main/Web.php';
require_once '../Main/Manifest.php';

$web = new Web();
$context = $web->context();
$captcha = $context->getSessionVar('captcha');
$sender = $context->getParameter("sender_mail");

if ($context->getParameter("captcha") === $captcha && filter_var($sender, FILTER_VALIDATE_EMAIL)) {

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: <' . $sender . '> ' . "\r\n";

    mail("chen.dzen@gmail.com", $context->getParameter("subject"), $context->getParameter("msg"), $headers);
    $context->setMessage("Съобщението е изпратено успешно !", MESSAGE_TYPE_SUCCESS);
} else {
    if($context->getParameter("captcha") != $captcha) {
        $context->setMessage("Невалиден код ! ".$captcha, MESSAGE_TYPE_ERROR);
    } else {
        $context->setMessage("Невалиден e-mail !", MESSAGE_TYPE_ERROR);
    }
}

header("Location:https://".DOMAIN."/aboutme.php");