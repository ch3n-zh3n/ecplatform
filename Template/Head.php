<head>
    <title><?php echo $web->getTitle(); ?></title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script language="javascript" type="text/javascript" src="<?php echo $root ?>JavaScript/main<?php echo ($context->isInDebuggingMode()) ? "" : "-min"?>.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $root ?>Design/css/main<?php echo ($context->isInDebuggingMode()) ? "" : "-min"?>.css">
</head>
