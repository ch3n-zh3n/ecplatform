<div class="background"></div>
<div id="header" class="header" >
    <div id="google_translate_element"></div><script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({pageLanguage: 'bg', layout: google.translate.TranslateElement.InlineLayout.VERTICAL, includedLanguages: 'en,de,ru'}, 'google_translate_element');
        }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <div class="logo unselectable"><?php echo DOMAIN; ?></div>
    <div class="menu-holder">
        <img class="unselectable <?php echo preg_match("/welcome/", $url) ? "active-home-button" : "home-button" ?>" width="23" src="<?php echo $root ?>Resources/images/design/home-w.png" title="Начало" onclick="window.location.replace('<?php echo $root; ?>');">
        <ul>
            <li>
                <img src="<?php echo $root; ?>Resources/images/glyphicons-w/png/glyphicons_156_show_thumbnails_with_lines.png">
                <a href="<?php echo $root ?>blog.php" class="after_load unselectable <?php if (preg_match("/blog/", $url)) echo "active"; ?>">КАТЕГОРИИ</a>
                <ul class="category-menu">
                    <li><p class="coupe-icon"></p><a href="#">КУПЕ</a></li>
                    <li><p class="sedan-icon"></p><a href="#">СЕДАН</a></li>
                    <li><p class="hatchback-icon"></p><a href="#">ХЕЧБЕК</a></li>
                    <li><p class="combi-icon"></p><a href="#">КОМБИ</a></li>
                    <li><p class="cabriolet-icon"></p><a href="#">КАБРИО</a></li>
                    <li><p class="pickup-icon"></p><a href="#">ПИКАП</a></li>
                    <li><p class="jeep-icon"></p><a href="#">ДЖИП</a></li>
                    <li><p class="van-icon"></p><a href="#">ВАН</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $root ?>AboutArdHome.php" class="after_load unselectable <?php if (preg_match("/(.*?)ArdHome(.*?)/", $url)) echo "active"; ?>">публикуване</a></li>
            <li><a href="<?php echo $root ?>projects.php" class="after_load unselectable <?php if (preg_match("/projects/", $url)) echo "active"; ?>">нови обяви</a></li>
            <li><a href="<?php echo $root ?>aboutme.php"  class="after_load unselectable <?php if (preg_match("/aboutme/", $url)) echo "active"; ?>">Търсене</a></li>
            <?php if ($account) { ?>
                <li>
                    <a href="" class="after_load unselectable <?php if (preg_match("/(.*?)Manage(.*?)/", $url)) echo "active"; ?>">Моят Профил</a>
                    <ul>
                        <li><a href="admin/accounts/new.php">Настройки</a></li>
                        <li><a href="admin/accounts/new.php">Моите обяви</a></li>
                        <li><a href="<?php echo $root ?>Controllers/LoginController.php?logout=true">Изход</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>           
    <div class="login-keys-holder">
        <?php if (!$account) { ?> 
            <img width="24px" src="<?php echo $root ?>Resources/images/design/keys.png"><a href="<?php echo $root ?>View/login.php">Акаунт</a>
        <?php } else { ?>
            <p>Здравей, <?php echo $account->getUsername(); ?></p>
        <?php } ?>
    </div>
</div>