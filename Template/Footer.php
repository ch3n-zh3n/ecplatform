<div class="footer">
    <div class="footer-blocks-holder">
        <div class="block">
            <img width="100px" src='<?php echo $root; ?>Resources/images/design/sslcert.png'>
        </div>
        <div class="block">
            <p>обслужване на клиенти</p>
            <ul>
                <li><a href="#">Правила и политики за бисквитки</a></li>
                <li><a href="#">Искате да се регистрирате като дилър ?</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Карта на сайта</a></li>
            </ul>
        </div>
        <div class="block">
            <p>Състояние</p>
            <ul>
                <li><a href="#">Нови автомобили</a></li>
                <li><a href="#">Употребявани </a></li>
                <li><a href="#">Катастрофирали</a></li>
                <li><a href="#">Повредени</a></li>
            </ul>
        </div>
        <div class="block">
            <p>за нас</p>
            <ul>
                <li><a href="#">Контакти</a></li>
                <li><a href="#">Политика</a></li>
                <li><a href="#">Сътрудници</a></li>
            </ul>
        </div>
    </div>
    <div class="social-container">
        <p>Последвайте ни в </p>
        <a class="face" target="_blank" href="https://www.facebook.com/webdesignfactory.eu/"></a>
        <a class="linkedin" target="_blank" href="#"></a>
        <a class="pinterest" target="_blank" href="#"></a>
        <a class="skype" target="_blank" href="#"></a>
    </div>       
    <hr>
    <font size="2.5em" class="unselectable">Webdesignfactory.eu™ 2016-2018 Created with <font color="red" size="3em">&#9825;</font> by WDF Team<br> All rights reserved &copy</font>
</div>
