<?php
require_once '../../Main/Account.php';
require_once '../../Main/DatabaseManipulator.php';
require_once '../../Main/Web.php';

$web = new Web();
$db = new DBManager();

$accountsArray = $db->getAccountsArray();
$context = $web->context();

$content = <<<EOS
        <h1>Управление на потребителски акаунти</h1>
        <table class="accounts-table">
        <tr>
            <th>ID</th>
            <th>Потребителско име</th>
            <th>Имейл</th>
            <th>Роля</th>
            <th>Идентификатор</th>
            <th>Състояние</th>
            <th>Промени</th>
        </tr>
EOS;

foreach($accountsArray as $acc) {
    $account = unserialize($acc);
    $id = $account->getId();
    $baseType = $account->getType();
    
    switch($baseType) {
        case ACCOUNT_TYPE_ADMIN:
                $select = <<<EOS
            <select id='role' onChange='saveFieldChanges(this,{$id},"type");'>
                <option value='0' selected='selected' >Администратор</option>
                <option value='1'>Модератор</option>
                <option value='2'>Потребител</option>
            </select>
EOS;
            break;
        case ACCOUNT_TYPE_MODERATOR:
                $select = <<<EOS
            <select id='role' onChange='saveFieldChanges(this,{$id},"type");'>
                <option value='1' selected='selected' >Модератор</option>
                <option value='0'>Администратор</option>
                <option value='2'>Потребител</option>
            </select>
EOS;
            break;
        case ACCOUNT_TYPE_USER:
                $select = <<<EOS
            <select id='role' onChange='saveFieldChanges(this,{$id},"type");'>
                <option value='2' selected='selected' >Потребител</option>
                <option value='0'>Администратор</option>
                <option value='1'>Модератор</option>                
            </select>
EOS;
            break;
    }
    
    $content .= "<tr id='".$account->getId()."'>";
    $content .= "<td>".$account->getId()."</td>";
    $content .= "<td headers='username' ondblclick='editField(this);'>".$account->getUsername()."</td>";
    $content .= "<td headers='email' ondblclick='editField(this);'>".$account->getEmail()."</td>";
    $content .= "<td>".$select."</td>";
    $content .= "<td>".$account->getUid()."</td>";
    $content .= "<td><font color='green' size='2em'>Активен</font></td>";
    $content .= "<td>"
            . "<img id='".$account->getId()."' src='/Resources/images/glyphicons/png/glyphicons_206_ok_2.png' title='редактиране'>&nbsp&nbsp"
            . "<img id='".$account->getId()."' src='/Resources/images/glyphicons/png/glyphicons_007_user_remove.png' title='премахване'>&nbsp&nbsp"
            . "<img id='".$account->getId()."' src='/Resources/images/glyphicons/png/glyphicons_240_rotation_lock.png' title='блокиране'>"
            . "</td>";
    $content .= "</tr>";
}
$content .= "</table>";


$web->contentWrapper($content);

require_once '../../MasterTemplate.php';