<?php
require_once 'Main/Web.php';
require_once 'Main/Account.php';

if (!isset($web)) {
    die("Web must be defined!");
}

// debugging mode must be disabled in production site 
$web->context()->enableDebuggingMode();

// Minifying only in debugging mode
$web->minify();
$context = $web->context();

$url = $context->uri();
$uriArray = $context->uri(true);

$account = $context->getAccount();
$root = $web->getRoot();

?>
<!DOCTYPE html>
<html lang="bg">
    <head>
        <title><?php echo $web->getTitle(); ?></title>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script language="javascript" type="text/javascript" src="<?php echo $root ?>JavaScript/main-min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo $root ?>Design/css/main-min.css">
    </head>
    <body>
        <div class="background"></div>
        <div id="header" class="header" >
            <div id="google_translate_element"></div><script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'bg', layout: google.translate.TranslateElement.InlineLayout.VERTICAL, multilanguagePage: true}, 'google_translate_element');
                }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <div class="logo unselectable"><?php echo DOMAIN; ?></div>
            <div class="menu-holder">
                <img class="unselectable <?php echo preg_match("/welcome/", $url) ? "active-home-button" : "home-button" ?>" width="23" src="/Resources/images/design/home-<?php echo preg_match("/welcome/", $url) ? "b" : "w"; ?>.png" title="Начало" onclick="window.location.replace('https://opcod3.com');">
                <ul>
                    <li><a href="<?php echo $root ?>blog.php" class="after_load unselectable <?php if (preg_match("/blog/", $url)) echo "active"; ?>">Блог</a></li>
                    <li>
                        <a href="<?php echo $root ?>AboutArdHome.php" class="after_load unselectable <?php if (preg_match("/(.*?)ArdHome(.*?)/", $url)) echo "active"; ?>">ArdHome</a>
                        <ul>
                            <li><a href="#">Garden</a></li>
                            <li><a href="#">Security</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo $root ?>projects.php" class="after_load unselectable <?php if (preg_match("/projects/", $url)) echo "active"; ?>">Проекти</a></li>
                    <li><a href="<?php echo $root ?>aboutme.php"  class="after_load unselectable <?php if (preg_match("/aboutme/", $url)) echo "active"; ?>">За мен</a></li>
                    <?php if ($account) { ?>
                        <li>
                            <a href="" class="after_load unselectable <?php if (preg_match("/(.*?)Manage(.*?)/", $url)) echo "active"; ?>">Администриране</a>
                            <ul>
                                <?php if ($account->getType() == 0) { ?>
                                    <li><a href="/Manage/accounts/control.php">Акаунти</a></li>
                                    <li><a href="admin/accounts/new.php">Проекти</a></li>                                
                                    <li><a href="admin/accounts/new.php">Логове</a></li>
                                <?php } elseif ($account->getType() == 1) { ?>
                                    <li><a href="admin/accounts/new.php">Публикации</a></li>
                                <?php } ?>
                                <li><a href="admin/accounts/new.php">Настройки</a></li>
                                <li><a href="/Handlers/LoginHandler.php?logout=true">Изход</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>           
            <div class="login-keys-holder">
                <?php if (!$account) { ?> 
                    <img width="24px" src="Resources/images/design/keys.png"><a href="login.php">Вход</a>
                <?php } else { ?>
                    <p>Здравей, <?php echo $account->getUsername(); ?></p>
                <?php } ?>
            </div>
        </div>
        <?php
        //show content;
        echo $web->getContent();
        ?>
        <div class="footer">
            <div class="social-container">
                <a class="face" target="_blank" href="https://www.facebook.com/zeko.nikolov?ref=bookmarks"></a>
                <a class="linkedin" target="_blank" href="https://www.linkedin.com/in/jeko-nikolov-18686429?trk=hp-identity-name"></a>
                <a class="git" target="_blank" href="https://github.com/ch3n-zh3n"></a>
                <a class="hacker_rank" target="_blank" href="https://www.hackerrank.com/chen_dzen?h_r=internal-search"></a>
            </div>       
            <hr>
            <font size="2.5em" class="unselectable">Opcod3.com™ 2016-2018 Created with <font color="red" size="3em">&#9825;</font> by ch3n-zh3n <br> All rights reserved &copy</font>
        </div>
        
        <?php if ($context->isInDebuggingMode()) { ?>
            <div id="placeholder"></div>
            <script>                
                document.getElementById("header").style['position'] = 'relative';
            </script>
        <?php } ?>
        <?php echo $web->messageMaker(); ?>
    </body>
</html>
