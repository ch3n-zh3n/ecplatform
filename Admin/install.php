<?php
require_once '../Core/Classes/DBManager.php';
require_once '../Core/Classes/Config.php';
require_once '../Core/Classes/Entities/Account.php';

$prefix = DB_PREFIX;

$create_accounts_table_query =<<<EOS

        CREATE TABLE IF NOT EXISTS `{$prefix}accounts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(50) NULL DEFAULT '0',
	`type` INT(11) NULL DEFAULT '0',
	`username` VARCHAR(50) NULL DEFAULT '0',
	`pass` VARCHAR(50) NULL DEFAULT '0',
	`email` VARCHAR(80) NULL DEFAULT '0',
	`ajc` VARCHAR(254) NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `username` (`username`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=6
;
EOS;
$db = new DBManager();
$err_code = $db->customQuery($create_accounts_table_query);

if($err_code != 0) {
    echo "Проблем с създаването на таблицата за потребители. MySQL error code (".$err_code.")";
    exit();
}

$account = new Account();
$account->setEmail("root@".DOMAIN);
$account->setPassword(strtoupper(md5(ROOT_PASS)));
$account->setType(ACCOUNT_TYPE_ADMIN);
$account->setUsername("root");

$err_code = $db->insertAccount($account);
if($err_code != 0) {
    echo "Проблем с добавянето на root потребителя. MySQL error code (".$err_code.")";
    exit();
}

echo "Инсталацията приключи успешно !";
?>