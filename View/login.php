<?php
require '../Core/Classes/Web.php';

$web = new Web();
$bc = $web->context()->getParameter("bad_credentials");
$root = $web->getRoot();

if($bc) {
    $error =<<<EOS
            <center>
                <p class='error'>
                    <img src='{$root}Resources/images/glyphicons/png/glyphicons_196_circle_exclamation_mark.png' class='icon'>
                    Грешно потребителско име или парола!
                </p>
            </center>
EOS;
} else {
    $error = "";
}
$content =<<<EOS
   <script>
        document.getElementsByClassName("background")[0].className = "background blur"; 
   </script>
   <div class='login_frame'>
       <span class='unselectable title'> Системен вход </span>
        <form action='{$root}Controllers/LoginController.php' method='POST' >
            <input class='after_load input ' type='text' placeholder='Потребителско име' name='username'><br>
            <input class='after_load input ' type='password' placeholder='Парола' name='password'>
        {$error}
        <hr>
        <p><a href='register.php'>Регистрирай се</a>, или си<a href='#'> забрави паролата</a> ? <br></p>
        <input class='button' type='submit' value='Влез'>
        </form>
   </div>
EOS;
$web->setContent($content);

require '../FrontOfficeTemplate.php';