<?php
require_once 'Core/Classes/Web.php';
require_once 'Core/Classes/Entities/Account.php';

if (!isset($web)) {
    die("Web must be defined!");
}

$context = $web->context();

// debugging mode must be disabled in production site 
$context->enableDebuggingMode();

// Minifying only in debugging mode
//$web->minify();

$url = $context->uri();
$uriArray = $context->uri(true);

$account = $context->getAccount();
$root = $web->getRoot();
?>
<!DOCTYPE html>
<html lang="bg">
    <?php
    require_once 'Template/Head.php';
    require_once 'Template/Header.php';
    ?>
    <body>
        <?php echo $web->getContent(); ?>
        <?php require_once 'Template/Footer.php'; ?>
        <div id="placeholder"></div>
        <?php echo $web->messageMaker(); ?>
    </body>
</html>
