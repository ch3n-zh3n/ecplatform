<?php
require_once 'Config.php';

class DBManager {

    private $connection;

   public function __construct() {
        $this->connection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    }

   public function getProjectsArray() {
        $sqlResult = mysqli_query($this->connection, "SELECT * FROM cz_projects");
        $projects = array();
        while ($row = mysqli_fetch_assoc($sqlResult)) {
            array_push($projects, $row);
        }
        return $projects;
    }

   public function getAccountsArray() {
        $sqlResult = mysqli_query($this->connection, "SELECT * FROM cz_accounts");
        $accounts = array();
        while ($row = mysqli_fetch_assoc($sqlResult)) {
            array_push($accounts, serialize($this->arrayToAccount($row)));
        }
        return $accounts;
    }

   public function getAccountByUsername($username) {
       $query = "SELECT * FROM ".DB_PREFIX."accounts WHERE `username`='" . $username . "';";
        $sqlResult = mysqli_query($this->connection, $query);
        if (mysqli_num_rows($sqlResult) === 1) {
            $acc = mysqli_fetch_assoc($sqlResult);
            return $this->arrayToAccount($acc);
        } else {
            return FALSE;
        }
    }
    
   public function insertAccount(Account $account) {
        $uid = strtoupper(md5($account->getUsername().$account->getType()));
        $account->setUid($uid);
        $query = "INSERT INTO ".DB_PREFIX."accounts (`uid`, `type`, `username`, `pass`, `email`) VALUES ('".$account->getUid()."', '".$account->getType()."', '".$account->getUsername()."', '".$account->getPassword()."', '".$account->getEmail()."')";
        mysqli_query($this->connection, $query);
        return mysqli_errno($this->connection);
    }
   
   public function customQuery($query) {
      mysqli_query($this->connection, $query);
      return mysqli_errno($this->connection);
   }
   
   public function getInstancee() {
       return $this->connection;
   }
    
   private function arrayToAccount($accArr) {
            $account = new Account();
            
            $account->setId($accArr['id']);
            $account->setAjc($accArr['ajc']);
            $account->setEmail($accArr['email']);
            $account->setUsername($accArr['username']);
            $account->setPassword($accArr['pass']);
            $account->setType($accArr['type']);
            $account->setUid($accArr['uid']);
            return $account;
    }
    
}
