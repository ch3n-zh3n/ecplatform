<?php

require_once "Context.php";
require_once "Config.php";

class Web {

    private $title;
    private $context;
    private $content;
    private $root = "";

    public function __construct() {
        $this->context = new Context();
        $uri = $this->context->uri(TRUE); 
        
        /* when using parent folder minus 2 */
        $p = (ROOT_FOLDER != "")? 3:1;
        
        for ($i = 0; $i < sizeof($uri) - $p; $i++) {
            $this->root .= '../';
        }
        
        $this->root .= ROOT_FOLDER."/";
        if (null != $this->context->getParameter("debug")) {
            $this->context->enableDebuggingMode();
        }
    }

    /**
     * 
     * @return type
     */
    public function getTitle() {
        if (empty($this->title)) {
            return DOMAIN;
        }
        return $this->title;
    }

    /**
     * 
     * @param type $title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * 
     * @return Context
     */
    public function context() {
        return $this->context;
    }

    /**
     * 
     * @param type $content
     */
    public function contentWrapper($content = "") {
        $this->content = "<div class='content_wrapper'>" . $content . "</div>";
    }

    /**
     * 
     * @return type
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * 
     * @param type $content
     */
    public function setContent($content) {
        $this->content = $content;
    }

    /**
     * 
     * @param type $content
     */
    public function appendContent($content) {
        $this->content .= $content;
    }

    public function messageMaker() {

        $state = $this->context();
        $msg = $state->popMessage();

        if (!$msg) {
            return false;
        }

        switch ($msg['type']) {
            case MESSAGE_TYPE_ERROR:
                $message = <<<EOS
                <div class="message-background"></div>
                      <div class="msg error-msg">
                            <div>Възникна грешка!</div>
                            <img src="{$this->root}Resources/images/glyphicons/png/glyphicons_308_bomb.png">
                            <p>{$msg['msg']}</p>
                      </div>
EOS;
                break;
            case MESSAGE_TYPE_NOTICE:
                $message = <<<EOS
                <div class="message-background"></div>
                      <div class="msg notice-msg">
                            <div>Системно събитие!</div>
                            <img src="{$this->root}Resources/images/glyphicons/png/glyphicons_195_circle_info.png">
                            <p>{$msg['msg']}</p>
                      </div>
EOS;
                break;
            case MESSAGE_TYPE_SUCCESS:
                $message = <<<EOS
                <div class="message-background"></div>
                      <div class="msg success-msg">
                            <div>Поздравление!</div>
                            <img src="{$this->root}Resources/images/glyphicons/png/glyphicons_198_ok.png">
                            <p>{$msg['msg']}</p>
                      </div>
EOS;
                break;
            case MESSAGE_TYPE_WARNING:
                $message = <<<EOS
                <div class="message-background"></div>
                      <div class="msg warning-msg">
                            <div>Внимание!</div>
                            <img src="{$this->root}Resources/images/glyphicons/png/glyphicons_205_electricity.png">
                            <p>{$msg['msg']}</p>
                      </div>
EOS;
                break;
        }
        return $message;
    }

    public function minify() {

        /*
         * //////////////////////// CSS Minifing begin ///////////////////////////////////////////////////////////////////////////
         */
        if ($this->context->isInDebuggingMode()) {
            $url = 'https://cssminifier.com/raw';
            $css = file_get_contents($this->root . 'Design/css/main.css');
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
                CURLOPT_POSTFIELDS => http_build_query(["input" => $css])
            ]);

            $minified = curl_exec($ch);
            curl_close($ch);

            file_put_contents($this->root . "Design/css/main-min.css", $minified);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /*
             * ///////////////////////// JS Minifing begin ////////////////////////////////////////////////////////////////////////
             */
            $url = 'https://javascript-minifier.com/raw';
            $js = file_get_contents($this->root . 'JavaScript/main.js');

            $data = array(
                'input' => $js,
            );

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $minified = curl_exec($ch);

            curl_close($ch);

            file_put_contents($this->root . "JavaScript/main-min.js", $minified);


            $debugging_log = file_get_contents("debugging_log");
            $debug_msg = "############################-". date("Y/m/d h:i:s") . "-##############################".PHP_EOL;
            $debug_msg .= " - css minified................";
            $debug_msg .= ($css_minified) ? "ok":"faild" . PHP_EOL;
            $debug_msg .= " - js minified.................";
            $debug_msg .= ($js_minified) ? "ok":"faild" . PHP_EOL;
            
            file_put_contents("debugging_log", $debugging_log . $debug_msg);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
    }
    
    /**
 * 
 * @return string
 */
    public function getRoot() {
        return $this->root;
    }

}
