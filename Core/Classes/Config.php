<?php

/* Global configuration */
define("ROOT_PASS","r1r2r3r4");
define("DOMAIN","ecplatform");
define("ROOT_FOLDER", "/ecplatform");

/* Database configuration */
define("DB_HOST", "127.0.0.1");
define("DB_USERNAME","root");
define("DB_PASSWORD","");
define("DB_PREFIX","ec_");
define("DB_NAME","ecplatform");