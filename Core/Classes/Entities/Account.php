<?php
define("ACCOUNT_TYPE_ADMIN", 0);
define("ACCOUNT_TYPE_MODERATOR", 1);
define("ACCOUNT_TYPE_USER", 2);

class Account {
    private $id;
    private $type;
    private $name;
    private $pass;
    private $uid;
    private $email;

    //(ajc) administrative json content;
    
    private $ajc;
    
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function getType() {
        return $this->type;
    }

    function getUsername() {
        return $this->name;
    }

    function getPassword() {
        return $this->pass;
    }

    function getUid() {
        return $this->uid;
    }

    function getEmail() {
        return $this->email;
    }

    function getAjc() {
        return $this->ajc;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setUsername($name) {
        $this->name = $name;
    }

    function setPassword($pass) {
        $this->pass = $pass;
    }

    function setUid($uid) {
        $this->uid = $uid;
    }

    function setEmail($email) {
        $this->email = $email;
    }
/**
 * 
 * @param JsonObject $ajc
 */
    function setAjc($ajc) {
        $this->ajc = $ajc;
    }

}