<?php

define("MESSAGE_TYPE_ERROR", 1);
define("MESSAGE_TYPE_NOTICE", 2);
define("MESSAGE_TYPE_WARNING", 3);
define("MESSAGE_TYPE_SUCCESS", 4);

class Context {

    private $parameters;
    private $debugging = FALSE;

    public function __construct() {
        session_start();
        $this->initializeParameters();
        if ($this->getParameter("debug")) {
            ini_set("display_errors", 1);
            error_reporting(E_ALL | E_STRICT);
            $this->debugging = TRUE;
        }
    }

    /**
     * 
     * @return type
     */
    public function getParameters() {
        return $this->parameters;
    }

    /**
     * 
     * @param type $param
     * @return type
     */
    public function getParameter($param) {
        if (isset($this->parameters[$param])) {
            return $this->parameters[$param];
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * @return type
     */
    public function isInDebuggingMode() {
        return $this->debugging;
    }

    /**
     * 
     * @param type $key
     * @return type
     */
    public function getSessionVar($key) {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return FALSE;
        }
    }

    /**
     * 
     * @param type $key
     * @param type $value
     */
    public function setSessionVar($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function enableDebuggingMode() {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->debugging = TRUE;
    }

    /**
     * 
     * @return type
     */
    public function protocol() {
        $proto = strtolower($_SERVER['REQUEST_SCHEME']);
        return $proto;
    }

    /**
     * 
     * @return type
     */
    public function port() {
        return $_SERVER['SERVER_PORT'];
    }

    /**
     * 
     * @return type
     */
    public function remoteAddress() {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * 
     * @return type
     */
    public function requestMethod() {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * 
     * @param type $toArray
     * @return type
     */
    public function uri($toArray = FALSE) {
        if ($toArray) {
            return array_filter(explode("/", $_SERVER['REQUEST_URI']));
        }
        return $_SERVER['REQUEST_URI'];
    }

    private function initializeParameters() {
        foreach ($_REQUEST as $key => $val) {
            $skey = filter_var($key, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $skey = filter_var($key, FILTER_SANITIZE_MAGIC_QUOTES);
            $sval = filter_var($val, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $sval = filter_var($val, FILTER_SANITIZE_MAGIC_QUOTES);

            $this->parameters[$skey] = $sval;
        }
    }

    /**
     * 
     * @param Account $account
     */
    public function setAccount($account) {
        $this->setSessionVar("account", serialize($account));
    }

    /**
     * 
     * @return Account
     */
    public function getAccount() {
        if (isset($_SESSION['account'])) {
            return unserialize($_SESSION['account']);
        } else {
            return false;
        }
    }

    public function removeAccount() {
        if (isset($_SESSION['account'])) {
            unset($_SESSION['account']);
        }
    }

    /**
     * 
     * @param string $message
     * @param int $type
     */
    public function putMessage($message, $type) {
        $_SESSION['message'] = Array(
            "msg" => $message,
            "type" => $type
        );
    }

    public function popMessage() {
        if (!empty($_SESSION['message'])) {
            $msg = $_SESSION['message'];
            unset($_SESSION['message']);
            return $msg;
        } else {
            return false;
        }
    }

}
